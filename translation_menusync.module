<?php

/**
 * @file
 * Extends menu item creation and syncronization in node add/edit form for
 * translations.
 */


/**
 * Implementation of hook_menu().
 */
function translation_menusync_menu(){
	$items['admin/build/menu/translation_menusync'] = array(
    'title' => 'Translation menu sync',
    'description' => t('Snycronization of node links of translated content in menus'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('translation_menusync_settings_form'),
    'file' => 'translation_menusync.admin.inc',
    'access arguments' => array('administer menu'),
    'type' => MENU_LOCAL_TASK,
  );
  return $items;
}

/**
 * This modules settings, with ensuring default values.
 */
function translation_menusync_settings() {
	static $set;
  // Static caching because of several different loads in code.
  if (empty($set)) {
    $set = variable_get('translation_menusync', array());
    if (!$set['parent_selection']) $set['parent_selection'] = 'default';
    if (!$set['creation']) $set['creation'] = 'default';
    if (!$set['deletion']) $set['deletion'] = 'default';
    if (!$set['deletion_mode']) $set['deletion_mode'] = 0;
    if (!$set['neutral']) $set['neutral'] = 'default';
  }
  return $set;
}



/**
 * Implementation of hook_form_alter().
 *
 * Adds menu alteration on node_edit_form.
 */
function translation_menusync_form_alter(&$form, $form_state, $form_id) {
 	if (isset($form['#node']) && $form_id == $form['#node']->type .'_node_form' & isset($form['menu'])) {
 		$set = translation_menusync_settings();
    $node = $form['#node'];
    if (!in_array($node->type, $set['node_types'])) return ;
    
    // Get language for this node
    $language = _translation_menusync_get_current('language', $node, $form_state, $_GET);
    // Get tnid.
    $tnid = _translation_menusync_get_current('tnid', $node, $form_state, $_GET);
    
    $form['language']['#default_value'] = $language;
    
    // Menu language
    $menu_language = ($language) ? $language : _translation_menusync_language_default($tnid);
    $menu_language_menu = $set['menus'][$menu_language];
    
    // Neutral language behaviour.
    // No menu item selection.
    if (!$language && $set['neutral'] == 'none') {
    	unset($form['menu']);
    }
    // Default behaviuor for netural items
    elseif(!$language && $set['neutral'] == 'default') {
    	//nothing means default.
    }
    // Language available for node menu item.
    elseif ($menu_language) {
      $form['#validate'][] = 'translation_menusync_node_form_validate';
      //$form['#submit'][] = 'translation_menusync_node_form_submit';
      // Get syncronized parent and weight for new node or no menu item yet.
      if (!$node->nid || !$node->menu['mlid'] || $set['parent_selection'] == 'force_sync_allways') {
      	$plid = _translation_menusync_plid_synced($tnid, $set[$menu_language_menu]);
        $weight = _translation_menusync_weight_synced($tnid);
        $form['menu']['parent']['#default_value'] = "$menu_language_menu:$plid";
        $form['menu']['weight']['#default_value'] = $weight;
      }
      // Deny selection of parent
      if ($node->tnid && $node->nid != $node->tnid && in_array($set['parent_selection'], array('force_sync_creation', 'force_sync_allways'))) {
      	$form['menu']['parent']['#disabled'] = TRUE;
        $form['menu']['weight']['#disabled'] = TRUE;
      }
      // Alter options
      $form['menu']['translation_menusync_language'] = array('#value' => $menu_language, '#type' => 'value');
      $form['menu']['parent']['#options'] = menu_parent_options(_translation_menusync_get_language_menus($language ? array($language) : _translation_menusync_get_untranslated($tnid)), $node->menu);
      
      // CREATION
      if ($set['creation'] == 'force_all' || ($language && $set['creation'] == 'force_transl')) {
      	$form['menu']['link_title']['#required'] = TRUE;
      }
      
      // DELETION
      switch ($set['deletion']) {
      	case 'never':
          $form['menu']['delete'] = array('#value' => 0, '#type' => 'value');          
        case 'only_all_source':
          $form['menu']['delete']['#disabled'] = ($node->nid && $node->nid == $node->tnid);
        case 'only_all':
          $form['menu']['delete']['#title'] = t('Delete all menu items of this node and its translations.');
          break;
      }
      
      // NEUTRAL: Additional neutral item titles.
      if (!$language && $set['neutral'] == 'multiple') {
        //TODO	
      }
    }
    else {
      // TODO: message for "no menu left"
    }
 	}
}

/**
 * Additional validation for node add/edit form.
 */
function translation_menusync_node_form_validate($form, &$form_state) {
	
  $language = _translation_menusync_get_current('language', $form['#node'], $form_state, $_GET);
  $tnid = _translation_menusync_get_current('tnid', $form['#node'], $form_state, $_GET);
  $menu_language = ($language) ? $language : _translation_menusync_language_default($tnid);
  $set = translation_menusync_settings();
  $menu_language_menu = $set['menus'][$menu_language];
  if ($menu_language != $form_state['values']['menu']['translation_menusync_language']) {
    $plid = _translation_menusync_plid_synced($tnid, $menu_language_menu);
    $form_state['values']['menu']['parent'] = "$menu_language_menu:$plid";
    form_set_error('menu][parent', t('The parent menu item options have changed. Please assure correct parent entry.'));
  }
}

/**
 * Implementation of hook_nodeapi().
 *
 * Changes the menu item on node prepare to provide the correct item in the
 * limited parent selection for the specific language menu.
 */
function translation_menusync_nodeapi(&$node, $op) {
	if ($op == 'prepare') {
		
    $set = translation_menusync_settings();
    if (!in_array($node->type,$set['node_types'])) return ;
    
    // Eventually get current menu entry as a default language fallback.
    if (isset($node->menu) && array_search($node->menu['menu_name'], $set['menus'])) {
    	$add_default_languages = array(array_search($node->menu['menu_name'], $set['menus']));
    }
    else {
    	$add_default_languages = array();
    }

    $menu_language = ($node->language) ? $node->language : _translation_menusync_language_default($node->tnid, $add_default_languages);
    // Assure menu item in correct menu and eventually update menu item 
    if ((empty($node->menu) || $node->menu['menu_name'] != $set['menus'][$menu_language]) && $node->nid) {
      // Prepare the node for the edit form so that $node->menu always exists.
      $menu_name = $set['menus'][$menu_language];
      $node->menu = translation_menusync_get_menu_item($menu_language, $node->nid, $node->tnid);
      // Find the new depth limit for the parent select.
      if (!isset($node->menu['parent_depth_limit'])) {
        $node->menu['parent_depth_limit'] = _menu_parent_depth_limit($node->menu);
      }
    }
	}
  elseif ($op == 'delete') {
    // Delete all translation_menusync module links that point to this node.
    $result = db_query("SELECT mlid FROM {menu_links} WHERE link_path = 'node/%d' AND module = 'translation_menusync'", $node->nid);
    while ($m = db_fetch_array($result)) {
      menu_link_delete($m['mlid']);
    }
  }
  elseif ($op == 'insert' || $op == 'update') {
  	$set = translation_menusync_settings();
    if (!in_array($node->type,$set['node_types'])) return ;
    $item = $node->menu;
    if ($item['delete'] && ($set['deletion'] == 'only_all' || $set['deletion'] == 'only_all_source')) {
    	if (in_array($set['deletion'], array('only_all', 'only_all_source'))) {
    		$tms_only = ($set['deletion_mode'] != 'all');
        $mlids = _translation_menusync_mlids($node->tnid, TRUE, $tms_only);
    	}
      else {
        $mlids = _translation_menusync_mlids($node->nid, FALSE, TRUE);
      }
      foreach ($mlids as $mlid) {
      	menu_link_delete($mlid);
      } 
    }
  }
}


/**
 * Retrieve a menu item for a node in a special language menu.
 * @param $language
 *   language code of the node language, that defines the menu language to look in
 * @param $nid
 *   the node id, might be 0 for new nodes
 * @param $tnid
 *   the node id of the source node, when available
 * @return
 *   specific menu item array or empty array
 */
function translation_menusync_get_menu_item($language, $nid, $tnid = 0) {
  $set = translation_menusync_settings();
  $menu_lang = $set['menus'][$language];
  $query = "SELECT m.mlid FROM {menu_links} m LEFT JOIN {node} n ON (m.link_path = CONCAT('node/', n.nid))";
  if ($nid && $tnid) {
    $query .= " WHERE m.menu_name = '%s' AND (n.nid = %d OR n.tnid = %d)";
    $args = array($menu_lang, $nid, $tnid);
  }
  elseif ($nid) {
    $query .= " WHERE m.menu_name = '%s' AND n.nid = %d";
    $args = array($menu_lang, $nid);
  }
  elseif ($tnid) {
    $query .= " WHERE m.menu_name = '%s' AND n.tnid = %d";
    $args = array($menu_lang, $tnid);
  }
  else {
    return array();
  }
  $query .= " ORDER BY (m.module = '%s') DESC";
  $args[] = 'translation_menusync';
  $mlid = db_result(db_query_range($query, $args, 0, 1));
 
  $item = (array) menu_link_load($mlid);
  return $item + array('link_title' => '', 'mlid' => 0, 'plid' => 0, 'menu_name' => $menu_lang, 'weight' => 0, 'options' => array(), 'module' => 'translation_menusync', 'expanded' => 0, 'hidden' => 0, 'has_children' => 0, 'customized' => 0);
}


/**
 * Retrieves a default language, that is not allready used as language for a
 * translated content of the node.
 * @param $tnid
 * @param $defaults
 *   languages that shall be tested first for default
 * @param $translation_exists
 *   checks for default language, whether to be not or allready a translated
 */
function _translation_menusync_language_default($tnid = NULL, $defaults = NULL, $translation_exists = FALSE) {
	if (!isset($defaults)) {
    // Get source node's language for first default
    if ($translation_exists) {
    	$tlangcode = db_result(db_query("SELECT language FROM {node} WHERE nid = %d", $tnid));
    }
    if ($tlangcode) {
    	$defaults = array($tlangcode, language_default('language'));
    }
    else {
    	$defaults = array(language_default('language'));
    }
	}
  if ($tnid) {
		$trans = translation_node_get_translations($tnid);
    // Run defaults
    foreach ($defaults as $language_default) {
    	if (isset($trans[$language_default]) == $translation_exists) {
        return $language_default;
      }
    }
    // Run all languages
    $languages = language_list('language');
    foreach ($languages as $langcode => $language) {
      if (isset($trans[$langcode]) == $translation_exists) return $langcode;
    }
    return FALSE;
	}
  else {
  	return current($defaults);
  }
}

/**
 * Retrieves a syncronized parent mlid.
 */
function _translation_menusync_plid_synced($tnid, $language, $source_language = NULL) {
  if (!$source_language) {
  	$source_language = _translation_menusync_language_default($tnid, NULL, TRUE);
  }
  $set = translation_menusync_settings();
  $source_menu = $set[$source_language];
  $menu  = $set[$language];
  $query = "SELECT m.mlid FROM" .
      " {menu_links} m" .
      " LEFT JOIN {node} n ON (m.link_path = CONCAT('node/', n.nid))" .
      " LEFT JOIN {node} sn ON (sn.tnid = n.tnid)" .
      " LEFT JOIN {menu_links} spm ON (spm.link_path = CONCAT('node/', sn.nid))" .
      " LEFT JOIN {menu_links} sm ON (spm.mlid = sm.plid)" .
      " WHERE m.menu_name = '%s'" .
      " AND sm.menu_name = '%s'" .
      " AND sn.tnid = %d" .
      " ORDER BY (sn.nid = sn.tnid) ASC," .
      " (sm.module = '%s') DESC," .
      " (spm.module = '%s') DESC"; // ORDER to give source node priority
  $args = array($menu, $source_menu, $tnid, 'translation_menusync', 'translation_menusync');
  return db_result(db_query_range($query, $args, 0 , 1));
}

/**
 * Get all menu mlids for a specific node.
 */
function _translation_menusync_mlids($nid = NULL, $as_tnid = TRUE, $tms_only = TRUE) {
  if ($as_tnid) {
		$query = "SELECT m.mlid" .
        " FROM {menu_link} m" .
        " LEFT JOIN {node} n ON (m.link_path = CONCAT('node/', n.nid))" .
        " WHERE n.tnid = %d";
	}
  else {
  	$query = "SELECT mlid FROM {menu_link} WHERE link_path = 'node/%d'";
  }
  if (!$tms_only) {
  	$res = db_query($query, $nid);
  }
  else {
  	$query .= " AND module = '%s'";
    $res = db_query($query, $nid, 'translation_menusync');
  }
  $return = array();
  while ($a = db_fetch_array($res)) {
  	$return[] = current($a);
  }
  return $return;
}

/**
 * Retrieve synced weight.
 */
function _translation_menusync_weight_synced($tnid, $source_language = NULL) {
	if (!$source_language) {
    $source_language = _translation_menusync_language_default($tnid, NULL, TRUE);
  }
  $set = translation_menusync_settings();
  $source_menu = $set[$source_language];
  $query = "SELECT weight FROM {menu_links} WHERE link_path = 'node/%d' AND menu_name = '%s'";
  return db_result(db_query_range($query,array($tnid, $source_menu),0, 1));
}

/**
 * Helper function to get current value.
 */
function _translation_menusync_get_current($type = 'language', $node, $form_state = array(), $get = array()) {
	switch ($type) {
		case 'language':
      if ($form_state['values']['language']) {
        $language = $form_state['values']['language'];
      }
      elseif (!$node->nid && $get['language']) {
        $language = $get['language'];
      }
      elseif ($node->language) {
        $language = $node->language;
      }
      else {
      	$language = "";
      }
      return $language;
      break;
    case 'tnid':
      if ($form_state['values']['tnid']) {
        $tnid = $form_state['values']['tnid'];
      }
      elseif (!$node->nid && $get['translation']) {
        $tnid = $get['translation'];
      }
      elseif ($node->tnid) {
        $tnid = $node->tnid;
      }
      elseif ($node->nid) {
        $tnid = $node->nid;
      }
      else {
        $tnid = 0;
      }
      return $tnid;
	}
}


/**
 * Retrieve untranslated languages for a tnid.
 */
function _translation_menusync_get_untranslated($tnid) {
	$trans = translation_node_get_translations($tnid);
  $set = translation_menusync_settings();
  $return = array();
  foreach ($set['menus'] as $langcode => $menu) {
    if (!isset($trans[$langcode])) {
      $return[] = $langcode;
    }
  }
  return $return;
}

/**
 * Retrieves menus for given languages.
 */
function _translation_menusync_get_language_menus($languages) {
	if (!is_array($languages)) {
		$languages = array($languages);
	}
  $set = translation_menusync_settings();
  $menus = menu_get_menus();
  $return = array();
  foreach ($languages as $langcode) {
  	if (!isset($trans[$langcode])) {
  		$return[$set['menus'][$langcode]] = $menus[$set['menus'][$langcode]];
  	}
  }
  return $return;
}

