
!!!!!!!! this README is in most parts OUT OF DATE!!!!!
take a look at SPECS.txt


This module adds functionality to synchronize menu items of selected node types
for each language. Therefore each language needs its own menu.
Once a menu entry is created for a node, any translated node will also get a menu
entry in its specified menu. Parent item relations will be adopted, if possible.

For language neutral content, menu items will only be set for languages content has
allready been translated to or for languages the user explicitly selected for the
node to use the neutral version.

If a translation is created after menu creation, the neutral entry would be overriden
or when no entry was present, a new entry would automatically be set.

Selection of the menu item parent is only possible in the source node. If it is a
language neutral node, the menu of the first language that uses the neutral one or
is allready translated, will be used. The current language and default language
would be preferred (in this order).

Configuration:
--------------
1) Specifiy menu for each language
on admin/build/menu/translation_menusync for each language a menu has to be activated.
Languages must not share one menu.

2) Specify node types
on admin/build/menu/translation_menusync the node types can be selected that should
have the alternate translation menusync instead of the core menu.


Setting menu entries:
---------------------

In source node:
* the "Menu link title" will be hidden
* the "Parent item" options will be restricted to the current language menu
* for each language there is a separat "menu link title"-field
* menu link title can only be set for
* * allready translated languages
* * languages using neutral language item (on neutral language source nodes)

In translated nodes:
* "Parent item", "delete item" checkbox and "weight" are hidden
* only menu link title could be altered, when at least one menu link item is present

For both:
* If a menu item is not anymore synced to the source node (parent item does not point on
the same node), then a checkbox may be present to resync.