<?php

/**
 * Settings form for module.
 */
function translation_menusync_settings_form(&$form_state) {
	$form = array();
  $set = translation_menusync_settings();
  $menus = menu_get_menus(TRUE);
  unset($menus['navigation']);
  $languages = language_list();
  if (count($menus) < count($languages)) {
  	drupal_set_message(t('There has to be at least one custom menu for each language. So please add additional menus at !link', array('!link' => l(t('Add menu'),'admin/build/menu/add',array('query' => array('destination' => 'admin/build/menu/translation_menusync'))))),'warning');
  }
  $form['_description'] = array(
    '#type' => 'markup',
    '#value' => t('Select a menu for each language. Items out of these menus will be synchronized with the menu structure of translated nodes in each other menu tree.'),  
  );
  $form['menus'] = array(
    '#type' => 'fieldset',
    '#title' => t('Menus'),
    '#suffix' => t('Links: !links', array('!links' => l(t('Create new menu'),'admin/build/menu/add',array('query' => array('destination' => 'admin/build/menu/translation_menusync'))))),
  );
  foreach ($languages as $langcode => $language) {
  	$form['menus'][$langcode] = array(
      '#type' => 'select',
      '#title' => $language->name,
      '#options' => $menus,
      '#default_value' => $set['menus'][$langcode],
      '#required' => TRUE,
    );
  }
  $deftext = "%default leaves original behaviour - without additional modules: %behaviour.";
  $form['node_types'] = array(
    '#type' => 'select',
    '#multiple' => TRUE,
    '#title' => t('Node types'),
    '#description' => t('Content types translation menusync shall override core menu functionality'),
    '#options' => node_get_types('names'),
    '#default_value' => (array)$set['node_types'],
  );
  $form['parent_selection'] = array(
    '#type' => 'radios',
    '#title' => t('Parent selection mode'),
    '#options' => array( 'default' => t('Default'), 'force_sync_creation' => t('Force sync on item creation'), 'force_sync_allways' => t('Force sync allways')),
    '#default_value' => $set['parent_selection'],
    '#description' => t($deftext, array('%default' => t('Default'), '%behaviour' => t('Selection possible for each singel language.'))),
  );
  $form['creation'] = array(
    '#type' => 'radios',
    '#title' => t('Creation of menu items'),
    '#options' => array( 'force_all' => t('Force creation of all languages'), 'force_transl' => t('Force creation of translated languages'), 'default' => t('Default')),
    '#default_value' => $set['creation'],
    '#description' => t($deftext, array('%default' => t('Default'), '%behaviour' => t('Optional creation'))),
  );
  $form['deletion'] = array(
    '#type' => 'radios',
    '#title' => t('Deletion of menu items'),
    '#options' => array( 'never' => t('Never'), 'only_all' => t('Only all at once'), 'only_all_source' => t('Only all at once in source node'), 'default' => t('Default')),
    '#default_value' => $set['deletion'],
    '#description' => t($deftext, array('%default' => t('Default'), '%behaviour' => t('Single deletion'))),
  );
  $form['deletion_mode'] = array(
    '#type' => 'radios',
    '#disabled' => !in_array($set['deletion'], array('only_all', 'only_all_source')),
    '#title' => t('Deletion mode'),
    '#default_value' => $set['deletion_mode'],
    '#description' => t('Select which items shall be deleted when  %only_all is selected', array('%only_all' => t('Only all'))),
    '#options' => array(0 => t('Only translation menusync items'), 'all' => t('All items')),
  );
  $form['neutral'] = array(
    '#type' => 'radios',
    '#title' => t('Settings for language neutral item creation'),
    '#options' => array( 'none' => t('None'), 'default' => t('Default'), 'multiple' => t('Multiple'), 'single' => t('Single')),
    '#default_value' => $set['neutral'],
    '#description' => t($deftext, array('%default' => t('Default'), '%behaviour' => t('Complete menu tree selection.'))),
  );
  return system_settings_form(array('translation_menusync' => $form, '#tree' => TRUE));
}

/**
 * Validation of admin form.
 */
function translation_menusync_settings_form_validate($form, &$form_state) {
  $menusyncs = $form_state['values']['translation_menusync']['menus'];
  if (count($menusyncs) != count(array_unique($menusyncs))) {
  	form_set_error('translation_menusync', t('A menu can only be assigned to one language. Please add a new menu at !link if you need an additional.', array('!link' => l(t('Add menu'),'admin/build/menu/add',array('query' => array('destination' => 'admin/build/menu/translation_menusync'))))));
  }
 // dpm($form_state);
}